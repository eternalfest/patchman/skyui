# 2.0.0 (2021-04-22)

- **[Breaking change]** Update to `patchman@0.10.4`.
- **[Internal]** Update to Yarn 2.

# 1.2.0 (2020-08-20)

- **[Fix]** Compatibility with patchman 0.9.2.

# 1.1.0 (2020-08-20)

- **[Feature]** Adding a text alignment parameter.

# 0.1.0 (2020-06-21)

- **[Feature]** First release.
