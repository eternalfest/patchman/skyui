package sky_ui;

import etwin.flash.MovieClip;
import etwin.Obfu;

import hf.mode.GameMode;
import hf.FxManager;
import hf.Hf;

enum TextAlignment {
    TextAlignmentLeft;
    TextAlignmentCenter;
    TextAlignmentRight;
}

class SkyUI {
    private static var old_darkness: Float;

    public static function PushDarknessAndLightSources(game: GameMode, new_darkness: Float) : Void {
        old_darkness = game.darknessMC._alpha;
        game.darknessMC._alpha = new_darkness;
        for (hole in game.darknessMC.holes)
            hole._visible = false;
        for (light_source in game.extraHoles)
            light_source.mc._visible = false;
    }

    public static function PopDarknessAndLightSources(game: GameMode) : Void {
        game.darknessMC._alpha = old_darkness;
        for (hole in game.darknessMC.holes)
            hole._visible = true;
        for (light_source in game.extraHoles)
            light_source.mc._visible = true;
    }

    public static function CreateNormalTextSprite(hf: Hf, game: GameMode, text: String, x: Float, y: Float, scale: Float, ?layer: Int, ?alignment: TextAlignment) : MovieClip {
        if (layer == null)
            layer = hf.Data.DP_TOP;
        if (alignment == null)
            alignment = TextAlignmentLeft;
        var text_sprite: Dynamic = game.depthMan.attach("hammer_interf_inGameMsg", layer);
        text_sprite.label.text = "";
        text_sprite.field.text = text;
        text_sprite.field._xscale = scale;
        text_sprite.field._yscale = scale;
        /* Avoids having to specify some arbitrary _width. */
        switch (alignment) {
            case TextAlignmentCenter:
                text_sprite.field.autoSize = Obfu.raw("center");
            case TextAlignmentRight:
                text_sprite.field.autoSize = Obfu.raw("right");
            case TextAlignmentLeft:
                text_sprite.field.autoSize = true; /* Equivalent to "left" but avoids having to Obfu.raw it. */
        }
        text_sprite.field._x = x;
        text_sprite.field._y = y;
        hf.FxManager.addGlow(text_sprite, 0, 2);
        return text_sprite;
    }
}